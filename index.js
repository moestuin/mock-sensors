const { interval, timer, fromEvent } = require('rxjs');
const { switchMap, map, throttleTime } = require('rxjs/operators');

const { connect } = require('async-mqtt');

const { RAB_MQ_MQTT_ADDRESS, RAB_MQ_USERNAME, RAB_MQ_PASSWORD } = process.env;

/**
 * Connect to MQTT
 */
const client = connect(RAB_MQ_MQTT_ADDRESS, {
    username: RAB_MQ_USERNAME,
    password: RAB_MQ_PASSWORD
});

/**
 * Listen to events from MQTT.js
 * @type {Observable<T>}
 */
const packetsSend$ = fromEvent(client, 'packetsend');
packetsSend$.subscribe(({payload, topic, messageId}) => console.log(topic, payload));

/**
 * Mock capacitive moist sensor
 * ---650--651--...--950--951|---650--651--...--950--951
 */
const sensorWateredValue = 650;
const sensorDryValue = 950;

const sunshineInterval = 300; // Change this if you want a faster emission of "Water needed" alerts
const wateringInterval = (sensorDryValue - sensorWateredValue) * sunshineInterval;
const sendOutDataThrottle = 2300;

// Plants are give water || it rains
const watering = timer(0, wateringInterval);

// Sunshine and plants are taking water
const sunshine = interval(sunshineInterval).pipe(
    map(x => sensorWateredValue + x),
    throttleTime(sendOutDataThrottle)
);

const source = watering.pipe(
    switchMap(x => sunshine)
);

// Send over MQTT
source.subscribe(sensorValue => client.publish('moes/moist', JSON.stringify({moist: sensorValue})));